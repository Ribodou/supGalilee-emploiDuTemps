# emploiDuTemps

Un programme python qui va chercher l'emploi du temps sur l'ENT de Sup Galilée et qui l'envoi par sms.

## Comment l'utiliser?

Creez un dossier "config" et placez-y un fichier "clefs.txt". Celui-ci doit contenir deux lignes, commançant par:

user=, suivi de votre nom d'utilisateur free.

password=, suivi de votre mot de passe.

### Exemple:
user=12345678

password=1234fHj4579586

Ensuite, lancez le programme:
### python3 calendrier.py