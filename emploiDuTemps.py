# -*- coding: utf-8 -*-

# Copyright (C) 2017 Lucas Robidou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# PS: For any question or any suggestion, please contact me at:
#luc.robidou@gmail.com

#a faire:
#gérer groupes (trop d'affichages de cours communs)
# vérifier si le programme n'a pas changé entre hier et aujourd'hui.


# Stratégie générale:
# 1) On télécharge le calendrier sur un site (ex: un Espce Numérique de
#    Travail, noté ENT ci-après)
# 2) On récupère les cours pertinents, c'est-à-dire les cours du jour
# 3) On extrait les informations utiles de ces cours(nom, salle, heure)
# 4) On envoit un sms

# ----- import des modules necessaires ---------------------------------

import wget  # domaine public
import os  # python
import requests  # apache 2.0
from ics import Calendar  # BSD
#from urllib.request import urlopen
import datetime  # python

# ----- définitions des fonctions --------------------------------------

def get_ids():
    """
        Récupère les identifiants contenus dans le fichier config/clefs.txt.

        :return: le nom d'utilisateur et le mot de passe
        :rtype: tuple of string
    """
    with open("config/clefs.txt") as f:
        contenu = f.readlines()

    if len(contenu) != 2:
        print("Le fichier n'a pas la bonne structure.")
        exit()

    user, password = "", ""
    for ligne in contenu:
        if ligne.startswith("user"):
            user = ligne.split("=")[1]
        if ligne.startswith("password"):
            password = ligne.split("=")[1]

    if user == "":
        print("Le nom d'utilisateur n'as pas été trouvé! :(")
        exit()
    if password == "":
        print("Le mot de passe n'as pas été trouvé! :(")
        exit()

    # on retire le "\n" à la fin s'il y en a un
    if user.endswith("\n"):
        user = user[:-1]
    if password.endswith("\n"):
        password = password[:-1]

    return user, password


def envoyer(sms):
    """
    	Send a message in a sms. You must be registered as a Free
    	customer and you must have activated this functionality.

    	:param a: The message you want to send.
    	:type a: string
    """
    user, password = get_ids()
    message = "https://smsapi.free-mobile.fr/sendmsg?"
    message += "user=" + user
    message += "&"
    message += "pass=" + password
    message += "&"
    message += "msg="

    #percent encoding du message
    for i in range(len(sms)):
    	if sms[i] == " ":
    		message += "%20"
    	elif sms[i] == "-":
    		message += "%2D"
    	else:
    		message += sms[i]
    requests.get(message)


def obtenir_nom_et_professeur(name):
    """
        Return a better name for the lesson.

        :param a: The ugly old horrible name
                (something like CODOGNET - Logique (G3SIL) - ING1 INFORMATIQUE)
        :type a: string
        :return: Two strings: the name of the professor
                              and the name of the lesson
        :rtype: tuple of strings
    """
    noms_cours = {"GS3IR": "Reseaux",
    "G3SISDD": "Structure de donnees",
    "G3SIAO": "Architecture des ordinateurs",
    "G3SGMPI": "mathematiques",
    "G3SISE": "Système d'exploitation",
    "G3SIBDD": "Bases de donnees",
    "G3SG2PS": "Probabilite et statistique",
    "G3SIPO": "Programmation Orientee Objet",
    "G3SITL": "Theorie des Langages",
    "G3SIL": "Logique",
    "G3SIC": "Compilation",
    "G3SISLP": "Sémantique des langages de programmation"
    }
    for clef in noms_cours:
        if clef in name:
            nom = noms_cours[clef]
            if "-" in name:
                nom_professeur = name.split("-")[0]
                while nom_professeur[0] == " ":
                    nom_professeur = nom_professeur[1:]
                while nom_professeur[-1] == " ":
                    nom_professeur = nom_professeur[0:-1]
                return nom, nom_professeur
    return None, None


def meilleur_debut(debut):
    """
    	Give you a better hour from a string.
        06:30:00+00:00 -> 06:30

    	:param a: A weird string. (ex: 07:30:00+00:00)
    	:type a: string
    	:return: Something like 07:30.
    	         (need to add an hour, see ajout_une_heure)
    	:rtype: string
    """
    debut = debut.split(":")[0] + ":" + debut.split(":")[1]
    return debut


def decaler_heure(heure, decalage):
    """
    	Remets les pendules à l'heure.

    	:param a: something like 07:30.
    	:type a: string
    	:return: Something like 08:30 (if decalage = 1).
    	:rtype: string
    """
    h = heure.split(":")[0]
    h = int(h)+decalage
    if h>=24:
    	h -= 24
    h = str(h)
    if len(h) == 1:
    	h = "0" + h
    heure = h + ":" + heure.split(":")[1]
    return heure


def obtention_date():
    """
    	Provide you the date.

    	:return: Something like "2017-11-10".(AAAA-MM-JJ)
    	:rtype: string
    """
    date = datetime.datetime.now()
    date = str(date).split(" ")[0]
    return date


def obtention_indice_cours(c, date):
    """
    	Give you indexes of today's lesson.

    	:param a: A calandar.
    	:type a: class 'ics.icalendar.Calendar'.
    	:param b: The date.
    	:type b: string
    	:return: liste of indice that is interesting.
    	:rtype: list of integer
    """
    liste_cours = []
    for i in range(len(c.events)):
    	d = c.events[i].begin
    	p = str(d).split("T")[0]
    	if (p == date):
    		liste_cours.append(i)
    return liste_cours


def creation_sms(calendrier, liste_indice_cours):
    """
    	Create a beautiful text message.

    	:param a: A calandar.
    	:type a: class 'ics.icalendar.Calendar'.
    	:param b: The list of the indice that is relevant for today.
    	:type b: list of integer
    	:return: A beautiful text message.
    	:rtype: string
    """
    sms = "Aujourd'hui, vous avez cours:%0D"
    for i, indice_cours in enumerate(liste_indice_cours):
        cour = calendrier.events[indice_cours]
        try:
            # nom du cours et nom du professeur
            nom, nom_professeur = obtenir_nom_et_professeur(cour.name)
            if (nom != None):
                sms +="de " + nom + "%0D" + "avec M. " + nom_professeur + "%0D"

            # heure de debut
            heure_debut_a_traiter = str(cour.begin).split("T")[1]
            heure_debut_lisible_decalee = meilleur_debut(heure_debut_a_traiter)
            heure_debut_lisible = decaler_heure(heure_debut_lisible_decalee, 1)  # heure d'hiver
            sms += "à partir de " + heure_debut_lisible + "%0D"

            # salle de cours
            salle = cour.location
            sms += "en " + salle + "%0D"
        except Exception as e:
            print(e)
            sms += "Un cour non traite." + "%0D"

        if i != len(liste_indice_cours)-1:
            sms +="-----%0D"
    return sms


if __name__ == "__main__":
    date = obtention_date()  # date = "2017-11-28"

    resultat_requete = requests.get("https://ent.univ-paris13.fr/ics/E75ZE").text
    calendrier = Calendar(resultat_requete)
    #trouvons les cours d'aujourd'hui
    liste_indice_cours = obtention_indice_cours(calendrier, date)
    sms = creation_sms(calendrier, liste_indice_cours)
    envoyer(sms)
    print(sms)
